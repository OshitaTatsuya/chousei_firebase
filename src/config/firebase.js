import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyB79TbIWxIeCh0c2mCF7xI1az0t2Bs8kb4",
  authDomain: "chousei-firebase-366c4.firebaseapp.com",
  databaseURL: "https://chousei-firebase-366c4.firebaseio.com",
  projectId: "chousei-firebase-366c4",
  storageBucket: "chousei-firebase-366c4.appspot.com",
  messagingSenderId: "524304232547",
  appId: "1:524304232547:web:6722a9d05f8a6ac0e78856",
  measurementId: "G-T2XNCBDBHL"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
